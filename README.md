# Talk-to-three-headed-monkey font

This font mimic the one used for verbs interface in
The Secret of Monkey Island and
Monkey island 2: Le Chuck's Revenge

https://gitlab.com/gromish/talk-to-three-headed-monkey-font/-/raw/master/image.png

Purpose
---
Keep in mind that this font mimic the one used [...].
Glyphs depend on the language (EN-FR-ES-DE-IT), so I had to make choices.

https://gitlab.com/gromish/talk-to-three-headed-monkey-font/-/raw/master/reference/languages.png


Easy way to contribute
---

1) Open ttt_12.sfd with FontForge
2) Edit glyphs: corrections, adds..
3) Element -> Font info (CTRL+SHIFT+F)
4) Element -> Regenerate bitmap glyphs (CTRL+B)
5) In general tab put 960 in em size (ascent becomes 800, descent becomes 160)
6) Save as ttt_960.sfd
7) File -> Generate Fonts (YES + BDF resolution 96)


Have fun!
